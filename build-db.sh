#!/usr/bin/env bash
#
# Script name: build-db.sh
# Description: Script for rebuilding the database for dtos-core-repo.
# GitLab: https://www.gitlab.com/JamesNova/core-repo-novaos
# Contributors: James Nova

# Set with the flags "-e", "-u","-o pipefail" cause the script to fail
# if certain things happen, which is a good thing.  Otherwise, we can
# get hidden bugs that are hard to discover.
set -euo pipefail

rm -rf x86_64/*

x86_pkgbuild=$(find ../pkgbuild-novaos/x86_64 -type f -name "*.pkg.tar.zst*")

for x in ${x86_pkgbuild}
do
    mv "${x}" x86_64/
    echo "Moving ${x}"
done

echo "###########################"
echo "Building the repo database."
echo "###########################"

## Arch: x86_64
cd x86_64
rm -f core-repo-novaos*

echo "###################################"
echo "Building for architecture 'x86_64'."
echo "###################################"

## repo-add
## -s: signs the packages
## -n: only add new packages not already in database
## -R: remove old package files when updating their entry
repo-add -n -R core-repo-novaos.db.tar.gz *.pkg.tar.zst

# Removing the symlinks because GitLab can't handle them.
rm core-repo-novaos.db
rm core-repo-novaos.files

# Renaming the tar.gz files without the extension.
mv core-repo-novaos.db.tar.gz core-repo-novaos.db
mv core-repo-novaos.files.tar.gz core-repo-novaos.files

cd ~/NovaRepos/pkgbuild-novaos
./clean-up.sh

echo "#######################################"
echo "Packages in the repo have been updated!"
echo "#######################################"
